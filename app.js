const url = require("url");
const fs = require("fs");
let path = require("path");

const filePath = "./index.html";
const serverLog  = require("./log.js")

const cities = require("cities");

var FAVICON = path.join(__dirname, "public", "favicon.ico");

module.exports = {
    handleRequest: function(request,response ) {
        const path = url.parse(request.url).pathname;
        var query = url.parse(request.url, true).query;
        var status = response.statusCode;
        console.log(path);
        serverLog.serverLog(path,query,status);

        switch (path) {
            case "/favicon.ico":
                response.setHeader("Content-Type", "image/x-icon");
                fs.createReadStream(FAVICON).pipe(response);
                response.end();
            case "/":
                var param = query.zipCode;
                if (param) {
                    if (cities.zip_lookup(param) != undefined) {
                        var city = cities.zip_lookup(param).city;
                        response.end(`The city you are looking for is: ${city}`);
                    }else{
                        response.statusCode = 404;
                        response.end("This zip is not found");
                    }
                }else{
                    if (fs.existsSync(filePath)) {
                        fs.readFile(filePath, function(err, data){
                            if (err) {
                                //status = 404;   
                                response.statusCode = 404;
                                response.end("File not found");
                            }
                            response.end(data);
                        })
                    }
                }
                break;
        
            default:
                response.statusCode = 404;
                response.end("This route doesn't exist");
        break;
        }
    }
}